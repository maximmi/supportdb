﻿/*
SQLyog Community v11.42 (64 bit)
MySQL - 5.5.23 : Database - support420
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`support420` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `support420`;

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `patronymic` varchar(50) DEFAULT NULL,
  `tarif` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(80),
  PRIMARY KEY (`id`),
  KEY `tarifs` (`tarif`),
  CONSTRAINT `tarifs` FOREIGN KEY (`tarif`) REFERENCES `tarifs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `clients` */

insert  into `clients`(`id`,`firstname`,`lastname`,`patronymic`,`tarif`,`phone`) values (1,'Максим','Метель','Юрьевич',2,'+79136788340'),(2,'Егор','Грязнов','Ракович',1,'+0123456789');

/*Table structure for table `operators` */

DROP TABLE IF EXISTS `operators`;

CREATE TABLE `operators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `patronymic` varchar(50) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(80) NOT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `types` (`type`),
  CONSTRAINT `types` FOREIGN KEY (`type`) REFERENCES `types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `operators` */

insert  into `operators`(`id`,`firstname`,`lastname`,`patronymic`,`type`,`phone`,`email`,`salary`) values (1,'Иван','Иванов','Иванович',3,'123456','ivanoff@dno.se',9000),(2,'Рак','Оленин','Крабович',1,'987654','clatz@dno.se',1000),(3,'Петр','Серегин','Иванович',2,'345566','iknow@dno.se',7000);

/*Table structure for table `queries` */

DROP TABLE IF EXISTS `queries`;

CREATE TABLE `queries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opid` int(11) NOT NULL,
  `clientid` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `date` date NOT NULL,
  `text` varchar(9000) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operators` (`opid`),
  KEY `clients` (`clientid`),
  KEY `statuses` (`status`),
  CONSTRAINT `operators` FOREIGN KEY (`opid`) REFERENCES `operators` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clients` FOREIGN KEY (`clientid`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `statuses` FOREIGN KEY (`status`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `queries` */

insert  into `queries`(`id`,`opid`,`clientid`,`title`,`date`,`text`,`status`) values (1,1,2,'Где интернет?','2016-02-29','Вчера вечером интернет пропал, до сих пор нету. Модем перезагружал, комп тоже. ЧЗХ?',2),(2,2,1,'Что со скоростью?','2016-02-20','Как-то 100кб-с вечером уже поднадоели. Я за что вам столько плачу?',1),(3,3,2,'Перевод от :1','2016-03-02','Выезжайте на :claddr, там кабель сперли',2);

/*Table structure for table `statuses` */

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `statuses` */

insert  into `statuses`(`id`,`text`) values (1,'Ожидает ответа'),(2,'В обработке'),(3,'Выполнен'),(4,'Выполнение невозможно');

/*Table structure for table `tarifs` */

DROP TABLE IF EXISTS `tarifs`;

CREATE TABLE `tarifs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `payment` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tarifs` */

insert  into `tarifs`(`id`,`name`,`payment`) values (1,'1мб-с',300),(2,'4мб-с',700),(3,'8мб-с',1100);

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TEXT` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `types` */

insert  into `types`(`id`,`TEXT`) values (1,'Первая линия'),(2,'Вторая линия'),(3,'Монтажник');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
