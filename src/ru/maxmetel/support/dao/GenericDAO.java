package ru.maxmetel.support.dao;

import java.util.List;

public interface GenericDAO<T> {
	
	public T insert(T client);

	public T getById(int id);

	public List<T> getAll();

	public void delete(T client);

	public void delete(int id);
	
	public void deleteAll();
	
	public void update(T client);

	public List<T> find(String[] fieldName, Object[] searchFor);
}
