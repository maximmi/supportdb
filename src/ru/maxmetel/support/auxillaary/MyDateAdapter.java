package ru.maxmetel.support.auxillaary;

import java.sql.Date;
import java.util.Calendar;

public class MyDateAdapter {

	public static String dateToString(Date date) {
		if (date==null) return null;
		StringBuilder tmp = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		tmp.append(calendar.get(Calendar.YEAR));
		tmp.append('-');
		tmp.append(calendar.get(Calendar.MONTH)+1);
		tmp.append('-');
		tmp.append(calendar.get(Calendar.DATE));
		return tmp.toString();
	}

}
