package ru.maxmetel.support.daoimpl;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ru.maxmetel.support.model.Client;
import ru.maxmetel.support.model.Operator;
import ru.maxmetel.support.model.Query;
import ru.maxmetel.support.model.Status;
import ru.maxmetel.support.model.Tarif;
import ru.maxmetel.support.model.Worktype;

public class HibernateUtils {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	public static SessionFactory buildSessionFactory() {
		try {
			Configuration conf = new Configuration();
			conf.addAnnotatedClass(Client.class);
			conf.addAnnotatedClass(Operator.class);
			conf.addAnnotatedClass(Query.class);
			conf.addAnnotatedClass(Status.class);
			conf.addAnnotatedClass(Tarif.class);
			conf.addAnnotatedClass(Worktype.class);
			return conf.configure().buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}