package ru.maxmetel.support.daoimpl;

import ru.maxmetel.support.model.Client;

public class ClientDAOImpl extends BaseDAOImpl<Client> {

	public ClientDAOImpl() {
		super(Client.class);
	}
	
}
