package ru.maxmetel.support.daoimpl;

import ru.maxmetel.support.model.Operator;

public class OperatorDAOImpl extends BaseDAOImpl<Operator> {
	public OperatorDAOImpl()
	{
		super(Operator.class);
	}
}
