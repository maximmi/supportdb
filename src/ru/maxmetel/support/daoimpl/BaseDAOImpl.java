package ru.maxmetel.support.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.model.BaseModel;

public class BaseDAOImpl<T extends BaseModel<?>> implements GenericDAO<T>{

	private Class<T> entityType;

	public BaseDAOImpl(Class<T> entityType) {
		this.entityType = entityType; 
	}
	
	protected Session getSession() {
		return HibernateUtils.getSessionFactory().openSession();
	}

	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	public T insert(T client) {
		try (Session session = getSession()) {
			session.beginTransaction();
			Integer id = (Integer) session.save(client);
			client.setId(id);
			session.getTransaction().commit();
			return client;
		}
	}

	public T getById(int id) {
		try (Session session = getSession()) {
			return session.get(entityType, id);
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		try (Session session = getSession()) {
			return session.createQuery("from "+entityType.getSimpleName()).list();
		}
	}

	public void delete(T client) {
		try (Session session = getSession()) {
			session.beginTransaction();
			session.delete(client);
			session.getTransaction().commit();
		}
	}

	public void delete(int id) {
		delete(getById(id));
	}
	
	public void deleteAll() {
		try (Session session = getSession()) {
			Query deleteclients = session.createQuery("DELETE FROM "+entityType.getSimpleName());
			deleteclients.executeUpdate();
		}
	}
	
	public void update(T client) {
		try (Session session = getSession()) {
			session.beginTransaction();
			session.update(client);
			session.getTransaction().commit();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(String[] fieldName, Object[] searchFor)
	{
		try (Session session = getSession()) {
			Criteria criteria = session.createCriteria(entityType);
			for (int i=1; i<searchFor.length; i++) {
				if (searchFor[i] != null) {
					criteria.add(Restrictions.like(fieldName[i], searchFor[i]));
				}
			}
			List<T> searchResults = criteria.list();
			return searchResults;
		}
	}

}