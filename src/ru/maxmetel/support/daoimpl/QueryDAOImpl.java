package ru.maxmetel.support.daoimpl;

import ru.maxmetel.support.model.Query;

public class QueryDAOImpl extends BaseDAOImpl<Query>{

	public QueryDAOImpl() {
		super(Query.class);
	}

}
