package ru.maxmetel.support.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

public class BabahForm extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5110519557500549308L;
	private final JPanel contentPanel = new JPanel();
	private boolean state = false;

	/**
	 * Create the dialog.
	 */
	public BabahForm(JFrame parent) {
		super(parent, true);
		setBounds(0, 0, 1280, 720);
		setTitle("�����? �����? ������� �����?");
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			BufferedImage icon;
			try {
				icon = ImageIO.read(new File("resources/2243911.jpg"));
				JLabel lblPic = new JLabel(new ImageIcon(icon));
				lblPic.setBounds(0, 0, 1280, 704);
				contentPanel.add(lblPic);
				lblPic.setHorizontalAlignment(SwingConstants.CENTER);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			
			JLabel label = new JLabel("\u0412\u044B \u0443\u0432\u0435\u0440\u0435\u043D\u044B, \u0447\u0442\u043E \u0445\u043E\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043B\u0438\u0442\u044C \u0412\u0421\u0415 \u0437\u0430\u043F\u0438\u0441\u0438 \u0438\u0437 \u0442\u0435\u043A\u0443\u0449\u0435\u0439 \u0442\u0430\u0431\u043B\u0438\u0446\u044B?");
			buttonPane.add(label);
			label.setHorizontalAlignment(SwingConstants.CENTER);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				okButton.addActionListener(ae -> {
					state = true;
					this.dispose();
				});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(ae -> this.dispose());
				getRootPane().setDefaultButton(cancelButton);
			}
		}
	}
	
	public boolean execute() {
 		this.setVisible(true);
     	return state;
 }
}
