package ru.maxmetel.support.swing;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;
import ru.maxmetel.support.model.BaseModel;
import ru.maxmetel.support.model.Client;
import ru.maxmetel.support.model.Operator;
import ru.maxmetel.support.model.Query;
import ru.maxmetel.support.model.Status;
import ru.maxmetel.support.model.Tarif;
import ru.maxmetel.support.model.Worktype;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class InsertForm<T extends BaseModel<?>> extends JFrame {

	private static final long serialVersionUID = -6235686547896799866L;
	private JPanel contentPane;
	private JTable table;
	private JButton btnOK;
	private Class<?> entityClass;
	private T entity;
	private JPanel panel;
	List<JLabel> labels = new ArrayList<>();
	List<JComboBox<? extends BaseModel<?>>> sels = new ArrayList<>();

	/**
	 * Create the frame.
	 */
	public InsertForm(String[] colnames, T entity) {
		this.entity = entity;
		this.entityClass = entity.getClass();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		btnOK = new JButton("OK");
		
		contentPane.add(btnOK, BorderLayout.SOUTH);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		initTable(table,colnames);
		initComboBoxes();
		btnOK.addActionListener(ae -> {
			insertData();
			initTable(table,colnames);
			});
		scrollPane.setViewportView(table);
	}
	
	public void initTable(JTable table, String[] colnames)
	{
		String cols[] = colnames;
		EnTableModel<T> dataModel = new EnTableModel<T>(cols);
		dataModel.addRow(entity);
		dataModel.setValueAt(entity.getId(), 0, 0);
		table.setModel(dataModel);
	}
	
	public void initComboBoxes()
	{
		labels.clear();
		sels.clear();
		switch(Tables.valueOf(entityClass.getSimpleName())) {
		case Client:
			JComboBox<Tarif> sel = (JComboBox<Tarif>) addComboBox(Tarif.class);
			final EnTableModel<Client> ppc = (EnTableModel<Client>)(table.getModel());
			sel.addActionListener(ae -> {
				ppc.getRow(0).setTarif((Tarif) sel.getModel().getSelectedItem());
				ppc.fireTableRowsUpdated(0, 0);
			}); 
			break;
		case Operator:
			JComboBox<Worktype> sel1 = (JComboBox<Worktype>) addComboBox(Worktype.class);
			final EnTableModel<Operator> ppc1 = (EnTableModel<Operator>)(table.getModel());
			sel1.addActionListener(ae -> {
				ppc1.getRow(0).setType((Worktype) sel1.getModel().getSelectedItem());
				ppc1.fireTableRowsUpdated(0, 0);
			}); 
			break;
		case Query:
			final EnTableModel<Query> ppc2 = (EnTableModel<Query>)(table.getModel());
			JComboBox<Client> sel2 = (JComboBox<Client>) addComboBox(Client.class);
			sel2.addActionListener(ae -> {
				ppc2.getRow(0).setClient((Client) sel2.getModel().getSelectedItem());
				ppc2.fireTableRowsUpdated(0, 0);
			}); 
			JComboBox<Operator> sel3 = (JComboBox<Operator>) addComboBox(Operator.class);
			sel3.addActionListener(ae -> {
				ppc2.getRow(0).setOperator((Operator) sel3.getModel().getSelectedItem());
				ppc2.fireTableRowsUpdated(0, 0);
			}); 
			JComboBox<Status> sel4 = (JComboBox<Status>) addComboBox(Status.class);
			sel4.addActionListener(ae -> {
				ppc2.getRow(0).setStatus((Status) sel4.getModel().getSelectedItem());
				ppc2.fireTableRowsUpdated(0, 0);
			}); 
			break;
		case Status: break;
		case Tarif: break;
		case Worktype: break;
		default:
			throw new RuntimeException("Unknown table "+entityClass.getSimpleName()+"!");
		}
		for(int i=0; i<sels.size(); i++)
		{
			panel.add(labels.get(i));
			panel.add(sels.get(i));
		}
	}
	
	public <E extends BaseModel<?>> JComboBox<?> addComboBox(Class<E> ent)
	{
		labels.add(new JLabel(ent.getSimpleName()));
		GenericDAO<E> dao = new BaseDAOImpl<E>(ent);
		List<E> tmp = dao.getAll();
		E[] items = (E[]) Array.newInstance(ent, tmp.size());
		items = dao.getAll().toArray(items);
		JComboBox<E> sel = new JComboBox<E>(items);
		sels.add(sel);
		return sel;
	}

	public void insertData()
	{
		GenericDAO<T> dao = new BaseDAOImpl<T>((Class<T>) entityClass);
		EnTableModel<T> model = (EnTableModel<T>)(table.getModel());
			for (int i=0; i<table.getModel().getRowCount(); i++)
			{
				T ent = (T)model.getRow(i); 
				if (dao.getById(ent.getId()) != null) dao.update(ent); 
					else dao.insert(ent);
			}
	}
	
}
