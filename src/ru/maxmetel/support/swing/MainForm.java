package ru.maxmetel.support.swing;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;

import ru.maxmetel.support.services.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;
import ru.maxmetel.support.daoimpl.HibernateUtils;
import ru.maxmetel.support.model.BaseModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class MainForm {

	private JFrame frame;
	protected static final Logger LOGGER = LoggerFactory.getLogger(MainForm.class);
	private JTable table;
	private JFrame insertForm = null;
	private JFrame editForm = null;
	private SearchForm<?> searchForm = null;
	private JLabel lblTableName;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
		initializeDB();
	}

	private void initializeDB() {
		try {
			HibernateUtils.buildSessionFactory();
		} catch (ExceptionInInitializerError ex) {
			LOGGER.error("Exteption while initializing Hibernate ORM: " + ex.getMessage());
			JOptionPane.showMessageDialog(null,"������: �� ������� ������������ � ������� ��");
			ex.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 527, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		JPanel actionPanel = new JPanel();
		frame.getContentPane().add(actionPanel);
		actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.X_AXIS));

		JButton btnShow = new JButton("Show");
		actionPanel.add(btnShow);

		JButton btnAdd = new JButton("Add");
		actionPanel.add(btnAdd);

		JButton btnEdit = new JButton("Edit");
		actionPanel.add(btnEdit);

		JButton btnDelete = new JButton("Delete");
		actionPanel.add(btnDelete);

		JButton btnClean = new JButton("Clean");
		actionPanel.add(btnClean);

		JButton btnFind = new JButton("Find");
		actionPanel.add(btnFind);

		JButton btnQuery = new JButton("Query");
		actionPanel.add(btnQuery);

		JComboBox<Tables> comboBox = new JComboBox<>();
		comboBox.setMaximumRowCount(6);
		comboBox.setModel(new DefaultComboBoxModel<Tables>(Tables.values()));
		comboBox.setSelectedIndex(0);
		actionPanel.add(comboBox);
		
		lblTableName = new JLabel("Table name");
		
		btnShow.addActionListener(ae -> {
			lblTableName.setText(comboBox.getSelectedItem().toString());
			table.setModel(Getter.getTable((Tables)(comboBox.getSelectedItem())));
		});
		
		btnAdd.addActionListener(ae -> {
			EnTableModel<?> model = (EnTableModel<?>) table.getModel();
			if (insertForm != null)
				insertForm.dispose();
			insertForm = createInsertForm(model,(Tables)(comboBox.getSelectedItem()));
			insertForm.setVisible(true);
		});
		
		btnDelete.addActionListener(ae -> 
		{
			EnTableModel<?> model = (EnTableModel<?>) table.getModel();
			if (table.getSelectedRow()<0)
				JOptionPane.showMessageDialog(null,"�������� ������� ��� ��������");
			else removeRow(table.getSelectedRow(),model,(Tables)(comboBox.getSelectedItem()));			
		});
		
		btnEdit.addActionListener(ae -> 
		{
			AbstractTableModel model = (AbstractTableModel) table.getModel();
			if (editForm != null)
				editForm.dispose();
			editForm = createEditForm((EnTableModel<?>)model,(Tables)(comboBox.getSelectedItem()),table.getSelectedRow());
			editForm.setVisible(true);
		});
		
		btnFind.addActionListener(ae -> 
		{
			AbstractTableModel model = (AbstractTableModel) table.getModel();
			searchForm = createSearchForm((EnTableModel<?>)model,(Tables)(comboBox.getSelectedItem()));
			if (searchForm.execute()) {
				processSearchResults(searchForm.getSearchResults());
			} else {
				JOptionPane.showMessageDialog(null, "pooquan on fire!");
			}
		});

		btnClean.addActionListener(ae -> 
			createBabahForm((Tables)(comboBox.getSelectedItem()))
		);
		
		btnQuery.addActionListener(ae -> {
			QueryForm queryForm = new QueryForm(frame);
			if (queryForm.execute()) {
				processSearchResults(queryForm.getQueryResults());
			}
		});
		
		JPanel tablePanel = new JPanel();
		frame.getContentPane().add(tablePanel);
		tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.Y_AXIS));

		tablePanel.add(lblTableName);

		table = new JTable();
		lblTableName.setLabelFor(table);
		tablePanel.add(new JScrollPane(table));
	}

	private void createBabahForm(Tables tableName) {
			BabahForm babahForm = new BabahForm(frame);
			if (babahForm.execute()) {
				GenericDAO<?> dynamite = new BaseDAOImpl<BaseModel<?>>((Class<BaseModel<?>>) Getter.entity(tableName));
				dynamite.deleteAll();
			} else System.out.println("psssshhh");
	}

	private <T extends BaseModel<?>> void processSearchResults(List<T> searchResults) {
		AbstractTableModel model = Getter.getTable(Tables.valueOf(searchResults.get(0).getClass().getSimpleName()));
		String[] colnames = ((EnTableModel<?>) model).getColumnNames();
		model = new EnTableModel<T>(searchResults,colnames);
		lblTableName.setText(searchResults.get(0).getClass().getSimpleName());
		table.setModel(model);
	}

	private void removeRow(int i, EnTableModel<?> model, Tables tableName)
	{
		GenericDAO<? extends BaseModel<?>> dao = new BaseDAOImpl<BaseModel<?>>((Class<BaseModel<?>>)Getter.entity(tableName));
		dao.delete(Integer.parseInt(model.getValueAt(i, 0).toString()));
		model.removeRow(i);
	}
	
	private InsertForm<?> createInsertForm(EnTableModel<?> model, Tables tableName) {
		try{
			return new InsertForm<BaseModel<?>>(model.getColumnNames(),(BaseModel<?>)Getter.entity(tableName).newInstance());
		} catch (IllegalAccessException | InstantiationException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
			throw new RuntimeException("Shit happened!");
		}
	}

	private InsertForm<?> createEditForm(EnTableModel<?> model, Tables tableName, int row) {
		try {
			return new InsertForm<BaseModel<?>>(model.getColumnNames(),(BaseModel<?>) model.getRow(row));
		} catch (ArrayIndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null,"�������� ������� ��� ��������������");
			throw e;
		}
	}

	private SearchForm<?> createSearchForm(EnTableModel<?> model, Tables tableName) {
		return new SearchForm<BaseModel<?>>(frame,model.getColumnNames(),Getter.entity(tableName));
	}
}
