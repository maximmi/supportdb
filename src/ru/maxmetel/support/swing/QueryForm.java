package ru.maxmetel.support.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.hibernate.Session;
import org.hibernate.criterion.Projections;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;
import ru.maxmetel.support.daoimpl.HibernateUtils;
import ru.maxmetel.support.model.BaseModel;
import ru.maxmetel.support.model.Client;
import ru.maxmetel.support.model.Operator;
import ru.maxmetel.support.model.Query;
import ru.maxmetel.support.model.Tarif;
import ru.maxmetel.support.services.CustomQueries;

import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import java.awt.GridLayout;
import java.lang.reflect.Array;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;

public class QueryForm extends JDialog {

	private boolean useSQL = false;
	private static final long serialVersionUID = 3306133197615909537L;
	private final JPanel contentPanel = new JPanel();
	private JTextArea textPane;
	private JComboBox<Tarif> tarifBox;
	private JComboBox<Client> clientBox;
	private JComboBox<Operator> operatorBox;
	private ButtonGroup qType;
	private List<? extends BaseModel<?>> results = null;
	private CustomQueries cqs = new CustomQueries();
	private JComboBox<String> topicBox;
	private JCheckBox checkBox;
	private boolean state = false;

	/**
	 * Create the dialog.
	 */
	public QueryForm(JFrame frame) {
		super(frame,true);
		setTitle("SQL query");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			textPane = new JTextArea();
			contentPanel.add(textPane);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.NORTH);
			panel.setLayout(new GridLayout(0, 2, 0, 0));
			{
				qType = new ButtonGroup();
				JRadioButton rdbtnTarif = new JRadioButton("\u041F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u0438 \u043F\u043E \u0442\u0430\u0440\u0438\u0444\u0443");
				rdbtnTarif.setActionCommand(Commands.CLIENT_BY_TARIF.toString());
				panel.add(rdbtnTarif);
				qType.add(rdbtnTarif);
				tarifBox = (JComboBox<Tarif>) addComboBox(Tarif.class);
				panel.add(tarifBox);
				JRadioButton clientButton = new JRadioButton("\u0417\u0430\u044F\u0432\u043A\u0438 \u043F\u043E \u043A\u043B\u0438\u0435\u043D\u0442\u0443");
				clientButton.setActionCommand(Commands.QUERY_BY_CLIENT.toString());
				panel.add(clientButton);
				qType.add(clientButton);
				clientBox = (JComboBox<Client>) addComboBox(Client.class);
				panel.add(clientBox);
				JRadioButton opButton = new JRadioButton("\u0417\u0430\u044F\u0432\u043A\u0438 \u043F\u043E \u043E\u043F\u0435\u0440\u0430\u0442\u043E\u0440\u0443");
				opButton.setActionCommand(Commands.QUERY_BY_OP.toString());
				panel.add(opButton);
				qType.add(opButton);
				operatorBox = (JComboBox<Operator>) addComboBox(Operator.class);
				panel.add(operatorBox);
				JRadioButton topicButton = new JRadioButton("\u0417\u0430\u044F\u0432\u043A\u0438 \u043F\u043E \u0442\u0435\u043C\u0435");
				topicButton.setActionCommand(Commands.QUERY_BY_TOPIC.toString());
				panel.add(topicButton);
				qType.add(topicButton);
				 Session session = HibernateUtils.getSessionFactory().openSession();
				 List<String> topicsList = session.createCriteria(Query.class).setProjection(Projections.property("title")).list();
				 String[] topics = new String[1]; 
				  topics = topicsList.toArray(topics);
				topicBox = new JComboBox<>(topics);
				panel.add(topicBox);
				checkBox = new JCheckBox("\u041D\u0435\u0437\u0430\u043A\u0440\u044B\u0442\u044B\u0435 \u0437\u0430\u044F\u0432\u043A\u0438");
				panel.add(checkBox);
				JRadioButton qlButton = new JRadioButton("\u0421\u0432\u043E\u0439 \u0437\u0430\u043F\u0440\u043E\u0441");
				qlButton.setSelected(true);
				panel.add(qlButton);
				qType.add(qlButton);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
				JRadioButton rdbtnHql = new JRadioButton("HQL");
				rdbtnHql.setSelected(true);
				rdbtnHql.setActionCommand("hql");
				rdbtnHql.addActionListener(ae -> useSQL = false);
				buttonPane.add(rdbtnHql);
				JRadioButton rdbtnSql = new JRadioButton("SQL");
				rdbtnSql.setActionCommand("sql");
				rdbtnSql.addActionListener(ae -> useSQL = true);
				buttonPane.add(rdbtnSql);			
			ButtonGroup ql = new ButtonGroup();
			ql.add(rdbtnHql);
			ql.add(rdbtnSql);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				okButton.addActionListener(ae -> {state  = performQuery(); this.dispose();});
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(ae -> this.dispose());
				buttonPane.add(cancelButton);
			}
		}
	}
	
	private boolean performQuery() {
		switch (Commands.valueOf(qType.getSelection().getActionCommand())) {
		case CLIENT_BY_TARIF:
			results = cqs.clientsByTarif((Tarif)tarifBox.getSelectedItem());
			break;
		case QUERY_BY_CLIENT:
			results = cqs.queriesByClient((Client)clientBox.getSelectedItem(), checkBox.isSelected());
			break;
		case QUERY_BY_OP:
			results = cqs.queriesByOperator((Operator)operatorBox.getSelectedItem(), checkBox.isSelected());
			break;
		case QUERY_BY_TOPIC:
			results = cqs.queriesByTopic((String)topicBox.getSelectedItem(), checkBox.isSelected());
			break;
		case CUSTOM:
			performCustomQuery();
			return false;
		default:
			JOptionPane.showMessageDialog(null, "Emmm... unknown query type");
		}
		return true;
	}

	public <E extends BaseModel<?>> JComboBox<?> addComboBox(Class<E> ent)
	{
		GenericDAO<E> dao = new BaseDAOImpl<E>(ent);
		List<E> tmp = dao.getAll();
		E[] items = (E[]) Array.newInstance(ent, tmp.size());
		items = dao.getAll().toArray(items);
		JComboBox<E> sel = new JComboBox<E>(items);
		return sel;
	}
	
	public boolean execute() {
		this.setVisible(true);
			
		return state;
	}

	private void performCustomQuery() {
		if (useSQL) JOptionPane.showMessageDialog(null,"���...");
		else {
			Session session = HibernateUtils.getSessionFactory().openSession();
			session.createQuery(textPane.getText()).executeUpdate();
		}
	}

	public List<? extends BaseModel<?>> getQueryResults() {
		return results;
	}
}
