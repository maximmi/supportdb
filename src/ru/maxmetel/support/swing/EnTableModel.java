package ru.maxmetel.support.swing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ru.maxmetel.support.model.BaseModel;

public class EnTableModel<T extends BaseModel<?>> extends AbstractTableModel {

	private static final long serialVersionUID = 7841797219196151964L;

	private final List<T> values;
	private final String[] colnames;
	private boolean[] isColumnEditable;

	public EnTableModel(List<T> values, String[] colnames) {
		super();
		this.values = values;
		this.colnames = colnames;
		this.isColumnEditable = new boolean[colnames.length];
		Arrays.fill(isColumnEditable, true);
		isColumnEditable[0] = false;
	}

	public EnTableModel(List<T> values) {
		this(values, null);
	}

	public EnTableModel(String[] colnames) {
		this(new ArrayList<T>(), colnames);
	}

	public EnTableModel() {
		this(new ArrayList<T>());
	}

	@Override
	public String getColumnName(int column) {
		if (colnames == null || column >= colnames.length)
			return super.getColumnName(column);
		else
			return colnames[column];
	}

	public String[] getColumnNames() {
		return this.colnames;
	}

	@Override
	public int getRowCount() {
		return values.size();
	}

	@Override
	public int getColumnCount() {
		return colnames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object[] row = values.get(rowIndex).toArray();
		return row[columnIndex];
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		getRow(rowIndex).set(columnIndex, aValue);
		this.fireTableCellUpdated(rowIndex, columnIndex);
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		Boolean isEditable = null;

		if (column < isColumnEditable.length)
			isEditable = isColumnEditable[column];

		return (isEditable == null) | isEditable;
	}

	public void setColumnEditable(int column, boolean isEditable) {
		isColumnEditable[column] = true;
	}

	@Override
	public Class<?> getColumnClass(int column) {
		if (getValueAt(0, column) == null)
			return Object.class;
		return getValueAt(0, column).getClass();
	}

	public T getRow(int index) {
		/* if (index != shadedRow) */ return values.get(index);
	}

	/*
	 * public T lightUp() { T entity = values.get(shadedRow); entity = (T)
	 * entity.fromArray(shade); values.set(shadedRow, entity); shadedRow = -1;
	 * return entity; }
	 */

	public void addRow(T value) {

		values.add(value);
		this.fireTableRowsInserted(this.getRowCount() - 1, this.getRowCount() - 1);
	}

	public void setRow(T value, int index) {
		values.add(index, value);
		this.fireTableDataChanged();
	}

	public void removeRow(int index) {
		values.remove(index);
		this.fireTableRowsDeleted(index, index);
	}
}
