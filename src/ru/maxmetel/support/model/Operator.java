package ru.maxmetel.support.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;

@Entity
@Table(name = "operators")
public class Operator extends BaseModel<Operator>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	@Column
	protected String firstName;
	@Column
	protected String lastName;
	@Column
	protected String patronymic;
	@ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="type")
	protected Worktype type;
	@Column
	protected String phone;
	@Column
	protected String email;
	@Column
	protected Integer salary;
	@OneToMany(cascade =  CascadeType.ALL,  mappedBy="operator", fetch = FetchType.EAGER)
	protected Set<Query> queries;
	
	public Object[] toArray()
	{
		Object[] buffer = new Object[8];
		buffer[0] = this.id;
		buffer[1] = this.lastName;
		buffer[2] = this.firstName;
		buffer[3] = this.patronymic;
		buffer[4] = this.phone;
		buffer[5] = this.email;
		buffer[6] = this.salary;
		buffer[7] = this.type;
		return buffer;
	}
	
	@Override
	public String toString()
	{
		return id+": "+lastName+" "+firstName;
	}

	public Operator fromArray(Object[] arr) {
		GenericDAO<Worktype> typeDAO = new BaseDAOImpl<Worktype>(Worktype.class);
		this.id = Integer.parseInt(arr[0].toString());
		this.lastName = arr[1].toString();
		this.firstName = arr[2].toString();
		this.patronymic = arr[3].toString();
		this.phone = arr[4].toString();
		this.email = arr[5].toString();
		this.salary = Integer.parseInt(arr[6].toString());
		this.type = typeDAO.getById(Integer.parseInt(arr[7].toString()));
		return this;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void set(int x, Object value) {
		GenericDAO<Worktype> typeDAO = new BaseDAOImpl<Worktype>(Worktype.class);
		switch (x)
		{
		case 0:	this.id = Integer.parseInt(value.toString());
		break; case 1: this.lastName = value.toString();
		break; case 2: this.firstName = value.toString();
		break; case 3: this.patronymic = value.toString();
		break; case 4: this.phone = value.toString();
		break; case 5: this.email = value.toString();
		break; case 6: this.salary = Integer.parseInt(value.toString());
		break; case 7: this.type = typeDAO.getById(Integer.parseInt(value.toString()));
		break; default: throw new RuntimeException ("Unknown index");
		}
	}

	public Worktype getType() {
		return type;
	}

	public void setType(Worktype type) {
		this.type = type;
	}
}
