package ru.maxmetel.support.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "types")
public class Worktype extends BaseModel<Worktype>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	@Column
	protected String text;
	@Override
	public String toString()
	{
		return id+": "+text;
	}
	@Override
	public Object[] toArray()
	{
		Object[] buffer = new Object[2];
		buffer[0] = this.id;
		buffer[1] = this.text;
		return buffer;
	}

	@Override
	public Worktype fromArray(Object[] arr) {
		this.id = Integer.parseInt(arr[0].toString());
		this.text = arr[1].toString();
		return this;
	}
	@Override
	public int getId() {
		return id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
	}
	

	@Override
	public void set(int x, Object value) {
		switch (x)
		{
		case 0:	this.id = Integer.parseInt(value.toString()); break;
		case 1: this.text = value.toString(); break;
		default: throw new RuntimeException ("Unknown index");
		}		
	}
}
