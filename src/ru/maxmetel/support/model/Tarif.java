package ru.maxmetel.support.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tarifs")
public class Tarif extends BaseModel<Tarif>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	@Column
	protected String name;
	@Column
	protected int payment;
	
	public Tarif(int id, String name, int payment) {
		super();
		this.id = id;
		this.name = name;
		this.payment = payment;
	}
	
	public Tarif() {
		this(0,"",0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + payment;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarif other = (Tarif) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (payment != other.payment)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return id + ": " + name + ": " + payment + ": �/���";
	}
	
	@Override
	public Object[] toArray()
	{
		Object[] buffer = new Object[3];
		buffer[0] = this.id;
		buffer[1] = this.name;
		buffer[2] = this.payment;
		return buffer;
	}
	
	@Override
	public Tarif fromArray(Object[] arr) {
		this.id = Integer.parseInt(arr[0].toString());
		this.name = arr[1].toString();
		this.payment = Integer.parseInt(arr[2].toString());
		return this;
	}
	
	@Override
	public int getId() {
		return id;
	}

    @Override
	public void set(int x, Object value) {
		switch (x)
		{
		case 0:	this.id = Integer.parseInt(value.toString()); break;
		case 1: this.name = value.toString(); break;
		case 2: this.payment = Integer.parseInt(value.toString()); break;
		default: throw new RuntimeException ("Unknown index");
		}		
	}
    
    @Override
    public void setId (int id) {
		this.id = id;
	}
	
	
}
