package ru.maxmetel.support.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.maxmetel.support.auxillaary.MyDateAdapter;
import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;
import ru.maxmetel.support.daoimpl.ClientDAOImpl;
import ru.maxmetel.support.daoimpl.OperatorDAOImpl;

@Entity
@Table(name = "queries")
public class Query extends BaseModel<Query>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	@ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="clientid")
	protected Client client;
	@ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="opid")
	protected Operator operator;
	@Column
	protected String title;
	@Column
	protected String text;
	@Column
	protected Date date;
	@ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="status")
	protected Status status;
	
	@Override
	public Object[] toArray()
	{
		Object[] buffer = new Object[7];
		buffer[0] = id;
		buffer[1] = client;
		buffer[2] = operator;
		buffer[3] = title;
		buffer[4] = text;
		buffer[5] = MyDateAdapter.dateToString(date);
		buffer[6] = status;
		return buffer;
	}

	@Override
	public Query fromArray(Object[] arr) {
		GenericDAO<Client> clDAO = new ClientDAOImpl();
		GenericDAO<Operator> opDAO = new OperatorDAOImpl();
		GenericDAO<Status> stDAO = new BaseDAOImpl<>(Status.class);
		this.id = Integer.parseInt(arr[0].toString());
		this.client = clDAO.getById(Integer.parseInt(arr[1].toString()));
		this.operator = opDAO.getById(Integer.parseInt(arr[2].toString()));
		this.title = arr[3].toString();
		this.text = arr[4].toString();
		this.date = Date.valueOf(arr[5].toString());
		this.status = stDAO.getById(Integer.parseInt(arr[6].toString()));
		return this;

	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public void set(int x, Object value) {
		switch (x)
		{
		case 0:	this.id = Integer.parseInt(value.toString()); break;
		case 1:
			GenericDAO<Client> clDAO = new ClientDAOImpl();
			this.client = clDAO.getById(Integer.parseInt(value.toString()));
			break;
		case 2:
			GenericDAO<Operator> opDAO = new OperatorDAOImpl();
			this.operator = opDAO.getById(Integer.parseInt(value.toString()));
			break;
		case 3: this.title = value.toString(); break;
		case 4: this.text = value.toString(); break;
		case 5: this.date = Date.valueOf(value.toString()); break;
		case 6: 
			GenericDAO<Status> stDAO = new BaseDAOImpl<>(Status.class);
			this.status = stDAO.getById(Integer.parseInt(value.toString()));	
			break;
		default: throw new RuntimeException ("Unknown index");
		}
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
