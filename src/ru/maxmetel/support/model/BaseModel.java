package ru.maxmetel.support.model;

public abstract class BaseModel<T> {
	
	abstract public int getId();
	abstract public void setId(int id);
	public abstract T fromArray(Object[] arr);
	public abstract Object[] toArray();
	public abstract void set(int x, Object value);
}
