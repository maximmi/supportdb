package ru.maxmetel.support.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;

@Entity
@Table(name = "clients")
public class Client extends BaseModel<Client>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	@Column
	protected String firstName;
	@Column
	protected String lastName;
	@Column
	protected String patronymic;
	@ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="tarif")
	protected Tarif tarif;
	@Column
	protected String phone;
	@Column
	protected String address;
	@OneToMany(cascade =  CascadeType.ALL,  mappedBy="client", fetch = FetchType.EAGER)
	protected Set<Query> queries;
	
	public Client(int id, String firstName, String lastName, String patronymic, Tarif tarif, String phone,
			Set<Query> queries, String address) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.patronymic = patronymic;
		this.tarif = tarif;
		this.phone = phone;
		this.queries = queries;
		this.address = address;
	}
	public Client(String firstName, String lastName, String patronymic, Tarif tarif, String phone,
			Set<Query> queries, String address) {
		this(0,firstName,lastName,patronymic,tarif,phone,queries,address);
	}
	public Client(String firstName, String lastName, String patronymic, Tarif tarif, String phone, String address) {
		this(0,firstName,lastName,patronymic,tarif,phone,new HashSet<Query>(),address);
	}
	
	public Client()
	{
		
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	public Tarif getTarif() {
		return tarif;
	}
	public void setTarif(Tarif tarif) {
		this.tarif = tarif;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Set<Query> getQueries() {
		return queries;
	}
	public void setQueries(Set<Query> queries) {
		this.queries = queries;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Object[] toArray()
	{
		Object[] buffer = new Object[7];
		buffer[0] = this.id;
		buffer[1] = this.lastName;
		buffer[2] = this.firstName;
		buffer[3] = this.patronymic;
		buffer[4] = this.phone;
		buffer[5] = this.tarif;
		buffer[6] = this.address;
		return buffer;
	}
	
	@Override
	public String toString()
	{
		return id+": "+lastName+" "+firstName;
	}
	
	public Client fromArray(Object[] arr) {
		GenericDAO<Tarif> tarifDAO = new BaseDAOImpl<Tarif>(Tarif.class); 
		this.id = Integer.parseInt(arr[0].toString());
		this.lastName = arr[1].toString();
		this.firstName = arr[2].toString();
		this.patronymic = arr[3].toString();
		this.phone = arr[4].toString();
		this.tarif = tarif == null ? null : tarifDAO.getById(Integer.parseInt(arr[5].toString()));
		this.address = arr[6].toString();
		return this;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public void set(int x, Object value) {
		GenericDAO<Tarif> tarifDAO = new BaseDAOImpl<Tarif>(Tarif.class);
		switch (x)
		{
		case 0:	this.id = Integer.parseInt(value.toString());
		break; case 1: this.lastName = value.toString();
		break; case 2: this.firstName = value.toString();
		break; case 3: this.patronymic = value.toString();
		break; case 4: this.phone = value.toString();
		break; case 5: this.tarif = tarif == null ? null : tarifDAO.getById(Integer.parseInt(value.toString()));
		break; case 6: this.address = value.toString(); 
		break; default: throw new RuntimeException ("Unknown index");
		}
	}
}
