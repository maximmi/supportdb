package ru.maxmetel.support.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;
import ru.maxmetel.support.daoimpl.HibernateUtils;
import ru.maxmetel.support.model.Client;
import ru.maxmetel.support.model.Operator;
import ru.maxmetel.support.model.Query;
import ru.maxmetel.support.model.Status;
import ru.maxmetel.support.model.Tarif;

public class CustomQueries {
	
	Session session = HibernateUtils.getSessionFactory().openSession();
	GenericDAO<Status> stDAO = new BaseDAOImpl<Status>(Status.class);
	
	public List<Client> clientsByTarif(Tarif tarif) {
		Criteria criteria = session.createCriteria(Client.class);
		criteria.add(Restrictions.like("tarif", tarif));
		return criteria.list();
	}
	
	public List<Query> queriesByClient(Client client, boolean notClosed) {
		Criteria criteria = session.createCriteria(Query.class);
		criteria.add(Restrictions.like("client", client));
		if (notClosed) criteria.add(Restrictions.like("status", stDAO.getById(1)));
		return criteria.list();
	}
	
	public List<Query> queriesByOperator(Operator operator, boolean notClosed) {
		Criteria criteria = session.createCriteria(Query.class);
		criteria.add(Restrictions.like("operator", operator));
		if (notClosed) criteria.add(Restrictions.like("status", stDAO.getById(1)));
		return criteria.list();
	}
	
	public List<Query> queriesByTopic(String topic, boolean notClosed) {
		Criteria criteria = session.createCriteria(Query.class);
		criteria.add(Restrictions.like("title", topic));
		if (notClosed) criteria.add(Restrictions.like("status", stDAO.getById(1)));
		return criteria.list();
	}
}
