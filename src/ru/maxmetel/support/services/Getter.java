package ru.maxmetel.support.services;

import java.util.List;

import javax.swing.table.AbstractTableModel;
import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.BaseDAOImpl;
import ru.maxmetel.support.daoimpl.ClientDAOImpl;
import ru.maxmetel.support.daoimpl.OperatorDAOImpl;
import ru.maxmetel.support.daoimpl.QueryDAOImpl;
import ru.maxmetel.support.model.BaseModel;
import ru.maxmetel.support.model.Client;
import ru.maxmetel.support.model.Operator;
import ru.maxmetel.support.model.Query;
import ru.maxmetel.support.model.Status;
import ru.maxmetel.support.model.Tarif;
import ru.maxmetel.support.model.Worktype;
import ru.maxmetel.support.swing.EnTableModel;
import ru.maxmetel.support.swing.Tables;

public class Getter {
	public static Class<? extends BaseModel<?>> entity(Tables tableName)
	{
		switch (tableName)
		{
		case Client:
			return Client.class;
		case Operator:
			return Operator.class;
		case Query:
			return Query.class;
		case Status:
			return Status.class;
		case Tarif:
			return Tarif.class;
		case Worktype:
			return Worktype.class;
		default:
			throw new RuntimeException("Unknown table!");
		}
	}
	
	public static AbstractTableModel getTable(Tables tableName) {
		GenericDAO<Client> clDAO = new ClientDAOImpl();
		GenericDAO<Operator> opDAO = new OperatorDAOImpl();
		GenericDAO<Query> qDAO = new QueryDAOImpl();
		GenericDAO<Status> stDAO = new BaseDAOImpl<Status>(Status.class);
		GenericDAO<Tarif> tarifDAO = new BaseDAOImpl<Tarif>(Tarif.class);
		GenericDAO<Worktype> typeDAO = new BaseDAOImpl<Worktype>(Worktype.class);
		AbstractTableModel model;
		
		String[] propertyNames;
	/*	ClassMetadata classMetadata = HibernateUtils.getSessionFactory().getClassMetadata(entity(tableName));
		String[] propertyNames = classMetadata.getPropertyNames();
		List<String> crutch = new ArrayList<>(Arrays.asList(propertyNames));
		crutch.remove("queries");
		propertyNames = crutch.toArray(propertyNames); */
		
		switch (tableName)
		{
		case Client:
			List<Client> clients = clDAO.getAll();
			propertyNames = new String[]{"id", "lastName", "firstName", "patronymic", "phone", "tarif", "address"};
			model = new EnTableModel<Client>(clients, propertyNames);
			break;
		case Operator:
			List<Operator> ops = opDAO.getAll();
			propertyNames = new String[]{"id", "lastName", "firstName", "patronymic", "phone", "email", "salary","type"};
			model = new EnTableModel<Operator>(ops, propertyNames);
			break;
		case Query:
			List<Query> queries = qDAO.getAll();
			propertyNames = new String[]{"id", "client", "operator", "title", "text", "date", "status"};
			model = new EnTableModel<Query>(queries, propertyNames);		
			break;
		case Status:
			List<Status> statuses = stDAO.getAll();
			propertyNames = new String[]{"id", "text"};
			model = new EnTableModel<Status>(statuses, propertyNames);		
			break;
		case Tarif:
			List<Tarif> tarifs = tarifDAO.getAll();
			propertyNames = new String[]{"id", "name", "payment"};
			model = new EnTableModel<Tarif>(tarifs, propertyNames);		
			break;
		case Worktype:
			List<Worktype> types = typeDAO.getAll();
			propertyNames = new String[]{"id", "text"};
			model = new EnTableModel<Worktype>(types, propertyNames);		
			break;
		default:
			throw new RuntimeException("Unknown table!");
		}
		return model;
	}

}
