package test;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;

import ru.maxmetel.support.dao.GenericDAO;
import ru.maxmetel.support.daoimpl.ClientDAOImpl;
import ru.maxmetel.support.daoimpl.HibernateUtils;
import ru.maxmetel.support.model.Client;
import ru.maxmetel.support.model.Query;

public class BaseDAOTests {
	protected GenericDAO<Client> clientDAO = new ClientDAOImpl();

	@BeforeClass()
	public static void init() {
		try {
			HibernateUtils.buildSessionFactory();
		} catch (ExceptionInInitializerError ex) {
			fail();
		}

	}

	@Before()
	public void clearDatabase() {
		clientDAO.deleteAll();
	}

	protected void checkClientFields1(Client author1, Client author2) {
		assertEquals(author1.getFirstName(), author2.getFirstName());
		assertEquals(author1.getLastName(), author2.getLastName());
		assertEquals(author1.getPatronymic(), author2.getPatronymic());
		assertEquals(author1.getTarif(), author2.getTarif());
		assertEquals(author1.getPhone(), author2.getPhone());
	}

	protected void checkAuthorFields(Client author1, Client author2) {
		checkClientFields1(author1, author2);
	}

	protected void checkClientQueries(Set<Query> queriesSet1, Set<Query> queriesSet2) {
		assertTrue(queriesSet1.equals(queriesSet2));
	}
}
